#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <Adafruit_MLX90614.h>
#include <SPI.h>
#include <SD.h>
const int chipSelect = 5;
File dataFile;
char filename[15] = "data_##.csv";

Adafruit_MLX90614 mlx = Adafruit_MLX90614();
static const int RXPin = 2, TXPin = 3;
static const uint32_t GPSBaud = 9600;

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);
//--------------------------
void initSD()
{
    Serial.println( F("Initializing SD card...") );
    for (uint8_t i=0; i<100; i++) {
      filename[5] = '0' + i/10;
      filename[6] = '0' + i%10;
      if (!SD.exists(filename)) {
        // Use this one!
        break;
      }
    }

    dataFile = SD.open(filename, FILE_WRITE);
    if (!dataFile) {
      Serial.print( F("Couldn't create ") ); 
      Serial.println(filename);
      while (true) {}
    }

    Serial.print( F("Writing to ") ); 
    Serial.println(filename);
    dataFile.close();

      // create table time,lat,lng,alt,RF
  Serial.println(F("card initialized."));
    dataFile = SD.open(filename, FILE_WRITE);
    String init = "lat,lng,amb,obj,del";
  if (dataFile) {
    dataFile.println(init);
    dataFile.close();
    // print to the serial port too:
    Serial.println(init);
  }
  else {
    Serial.println("error opening "+String(filename));
  }
} // initSD



void setup(){
  Serial.begin(9600);
  ss.begin(GPSBaud);

  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    while (1);
  }
  if (!mlx.begin()) {
    Serial.println("Error connecting to MLX sensor. Check wiring.");
    while (1);
  };
  initSD();
}

void loop(){
  String line = "";
  String lat = "";
  String lng = "";
  String max = "";
  // This sketch displays information every time a new sentence is correctly encoded.
  while (ss.available() > 0){
    gps.encode(ss.read());
    if (gps.location.isUpdated()){    
      lat = String(gps.location.lat(), 6); 
      lng = String(gps.location.lng(), 6); 
      line = lat+","+lng+","+String(mlx.readAmbientTempC())+","+String(mlx.readObjectTempC())+","+String(mlx.readObjectTempC()-mlx.readAmbientTempC());  
      Serial.println(line);
      dataFile = SD.open(filename, FILE_WRITE);
      if (dataFile) {
        dataFile.println(line);
        dataFile.close();
      }
      
    }
  }
}
